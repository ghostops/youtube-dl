#YouTube-DL PHP#
YouTube-DL is a service built on the popular youtube-dl script and allows users to download, convert, recode and mirror YouTube-videos.
You can (if you want) host this yourself, the sourcecode is on BitBucket, tough not always up to date.
Everything on this site (except the videos) is licenced under the MIT License.

##This script comes "AS IS"##
That means it might be buggy, it might be inefficient, it might break, and it might flat out not work. If that is the case, feel free to try and fix it and submit a pull request!