<?php 
    //POST
    $url = $_POST['url'];
    $audioformat = "mp3";
    $rand = $_POST['publicID'];

   if (strpos($url,'&')) {
        echo "<div class='error-box'>Sorry, we do not support playlists yet :(<br>If this was not a playlist link, try copy-pasting it again!</div>";
        exit(0);
    }

    //Define
    $result = array();
    $status;
    //Admin set vars
    if (filter_var($url, FILTER_VALIDATE_URL)) {
        mkdir("random/$rand", 0777);
    }

    //Define url and string to execute
    $string = 'youtube-dl "'.$url.'" -x --audio-format "mp3" -o "/var/www/html/ytdl/mp3/random/'.$rand.'/%(id)s.%(ext)s"';

    //Open pipe to recive errors and output
    $descriptorspec = array(
       0 => array("pipe", "r"),  // stdin
       1 => array("pipe", "w"),  // stdout
       2 => array("pipe", "w"),  // stderr
    );
    //Execute
    $process = proc_open($string, $descriptorspec, $pipes);

    //Read from stddout and stderr
    $stdout = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[2]);
    $ret = proc_close($process);

    //Generate JSON Array
    json_encode(array('status' => $ret, 'errors' => $stderr,'url_orginal'=>$url, 'output' => $stdout,'command' => $string));
    //Execute and generate JSON array
    //exec($string, $result, $status);

    //Get Metadata
    $title = exec("youtube-dl $url -e");
    //$json = json_encode(array('status' => $status, 'url_orginal'=>$url, 'url' => $result, 'command' => $string));

    //Get Youtube ID from download URL
    $youtubeID = substr($url, strpos($url, "=") + 1);    

        //Display URL based on filetype 
        $serverURL = $_SERVER['REQUEST_URI']; //Returns the current URL
        $parts = explode('/',$serverURL);
        $dir = $_SERVER['SERVER_NAME'];
        for ($i = 0; $i < count($parts) - 1; $i++) {
            $dir .= $parts[$i] . "/";
        }
        $startlink = "http://$dir";

        $audiolinkformat = "random/$rand/";
        $filelink = "$youtubeID.$audioformat";

        //Create link
        $filedownload = $startlink.$audiolinkformat.$filelink;

    //Echo stuff
    if (empty($stderr)) {
        echo "
        <div class='dl-content'>
        <h2>$title</h2>
        <a class='dl-link' href='$filedownload' download><span>Download</span></a>
        </div>
        ";
    }
    elseif (strpos($stderr,'requested')) {
        echo "
        <div class='dl-content'>
        <h2>$title</h2>
        <div class ='hd-error'>No MP3 avalible! Please try another video.</div>
        </div>
        ";
    }
    else {
        echo "<div class='error-box'><br>$stderr<br></div>";
    }

?>