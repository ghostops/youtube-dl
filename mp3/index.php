<?php
  //Generate publicID
  $publicID = substr(md5(rand()), 0, 7);

  //Get server URL
  $serverURL = $_SERVER['REQUEST_URI']; //Returns the current URL
  $parts = explode('/',$serverURL);
  $dir = $_SERVER['SERVER_NAME'];
  for ($i = 0; $i < count($parts) - 1; $i++) {
      $dir .= $parts[$i] . "/";
  }
  $startlink = "http://$dir";

  $folderURL = $startlink.'random/'.$publicID;

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>YTDL - MP3</title>
  <link rel="stylesheet" type="text/css" href="../res/loading.css">
  <link rel="stylesheet" type="text/css" href="../res/animate.css">
  <link rel="stylesheet" type="text/css" href="style.css">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <!--Ajax-->
  <script>
  $(function() {
    $("#dl-form").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function() {
                $("#loader").toggleClass("toggle");
            },
            success: function(data) {
                $("#loader").toggleClass("toggle");
                $("#response").append(data);
                $(".zipAction").fadeIn();
            }
        });
    });
  });

  $(function() {
  $("#zipit").on("submit", function(e) {
      e.preventDefault();
      $.ajax({
          url: $(this).attr("action"),
          type: 'POST',
          data: $(this).serialize(),
          beforeSend: function() {
              $("#loader").toggleClass("toggle");
          },
          success: function(data) {
              $("#loader").toggleClass("toggle");
              $(data).insertBefore("#response .dl-content:first");
          }
      });
  });
});
  </script>
</head>
<body>
  <div id="loader">
  <!--<div class="loader-message">This is a message!</div>-->
  <div class='cover'></div>
  <div class='tetrominos'><div class='tetromino box1'></div><div class='tetromino box2'></div><div class='tetromino box3'></div><div class='tetromino box4'></div></div>
  </div>
  <header>
    <a href="/ytdl"><h1><span>YouTube</span> - <strong>MP3</strong></h1></a>
  </header>

  <nav>
    <ul>
      <li><a href="/ytdl">HOME</a></li>
      <li><a href="#">CONVERT MP3</a></li>
      <li><a href="../mirror">MIRROR</a></li>
    </ul>
  </nav>

  <main>
    <div class="publicID"><span><span class="before">Your Public ID, destroys on refresh: </span><?php echo $publicID ?></span><span class="aboutID">?</span></div>
    <div class="mainInner">
      <form method="POST" action="download-mp3.php" id="dl-form" class="clearfix">
        <!--URL-->
        <input id="url" type="url" placeholder="YouTube URL" name="url" required>

        <!--ID-->
        <input class="hidden-forms" type="text" name="publicID" value="<?php echo $publicID ?>"><!--You will only fuck your shit up if you change this...-->
        
        <!--AUDIO-->
        <input type="checkbox" id="audio-on" name="audio" value="true" checked>
        <input type="checkbox" id="audioformat" name="audioformat" value="mp3" checked>


        <!--SUBMIT-->
        <input id="submit" type="submit" value="Convert to MP3">
      </form>
      <div class="zipAction">
        <form id="zipit" name="zipit" action="ZipIt.php">
          <!--ZipIt!-->
          <input class="actionButton" id="zip-download" type="submit" value="" title="Download All Converted Files">
          <input class="hidden-forms" type="text" name="publicID" value="<?php echo $publicID; ?>">
        </form>
        <button class="actionButton" id="url-link" title='Link to all converted files'></button>
        <button class="actionButton" id="clearFeed" title="Clear the download feed"></button>
      </div>
    </div>
  </main>

  <section id="response" class=""></section>

  <section id="about">
    <div class="aboutInner">
      <?php include ("../res/about.php"); ?>
      <br><a class="builtBy" href="http://ghostops.nu">Built by Ludvig "GhostOps" Larsendahl</a>
    </div>
  </section>

  <!--Casual JQuery-->
  <script src="../res/wow.js"></script>
  <script>
  //Init WOW
  new WOW().init();         

  //Events
  $(document).ready(function(){
      $('#dl-form').submit(function(){
          $('.publicID').addClass("toggle");
      });
      //Get DL-Link
      $('#url-link').click(function(){
          $("<div class='urlBox'><a href='<?php echo  $folderURL  ?>'><?php echo  $folderURL  ?></a></div>").insertBefore("#response .dl-content:first");
      });
      //About ID
      $('.aboutID').click(function(){
          $("#response").append("<div class='aboutIDBox'><div class='inner'><h2>What's your Public ID?</h2><p><?php include ('../res/publicIDinformation.php'); ?></p></div></div>");
      });
      //Clear Response
      $('#clearFeed').click(function(){
          $('#response').fadeOut(400, function() {
             $(this).empty().show();
          });
      });
  });
  </script>
</body>
</html>