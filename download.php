<?php 
    //POST
    $url = $_POST['url'];
    $hd = $_POST['hd'];
    $videoformat = $_POST['videoformat'];    
    $ifaudio = $_POST['audio'];
    $audioformat = $_POST['audioformat'];

   if (strpos($url,'&')) {
        echo "<div class='error-box'>Sorry, we do not support playlists yet :(<br>If this was not a playlist link, try copy-pasting it again!</div>";
        exit(0);
    }

    //Define
    $result = array();
    $status;
    //Admin set vars

    if ($ifaudio == true) {
      //Define url and string to execute
      $string = 'youtube-dl "'.$url.'" -x --audio-format "'.$audioformat.'" -o "/var/www/html/ytdl/audio/%(id)s.%(ext)s"';
    }
    elseif ($hd == true) {
      $string = 'youtube-dl "'.$url.'" -f "137+140" -o "/var/www/html/ytdl/video/%(id)s.%(ext)s"';
    }
    elseif ($videoformat != "mp4") {
      $string = 'youtube-dl "'.$url.'" -f "22+140" --recode-video "'.$videoformat.'" -o "/var/www/html/ytdl/video/%(id)s.%(ext)s"';
    }
    else {
      //Define url and string to execute
      $string = 'youtube-dl "'.$url.'" -f "22+140" -o "/var/www/html/ytdl/video/%(id)s.%(ext)s"';
    }

    //Open pipe to recive errors and output
    $descriptorspec = array(
       0 => array("pipe", "r"),  // stdin
       1 => array("pipe", "w"),  // stdout
       2 => array("pipe", "w"),  // stderr
    );
    //Execute
    $process = proc_open($string, $descriptorspec, $pipes);

    //Read from stddout and stderr
    $stdout = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[2]);
    $ret = proc_close($process);

    //Generate JSON Array
    json_encode(array('status' => $ret, 'errors' => $stderr,'url_orginal'=>$url, 'output' => $stdout,'command' => $string));
    //Execute and generate JSON array
    //exec($string, $result, $status);
    //Get Metadata
    $thumbnail = exec("youtube-dl $url --get-thumbnail");
    $title = exec("youtube-dl $url -e");
    //$json = json_encode(array('status' => $status, 'url_orginal'=>$url, 'url' => $result, 'command' => $string));

    //Get Youtube ID from download URL
    $youtubeID = substr($url, strpos($url, "=") + 1);    

        //Display URL based on filetype 
        $serverURL = $_SERVER['REQUEST_URI']; //Returns the current URL
        $parts = explode('/',$serverURL);
        $dir = $_SERVER['SERVER_NAME'];
        for ($i = 0; $i < count($parts) - 1; $i++) {
            $dir .= $parts[$i] . "/";
        }
        $startlink = "http://$dir";
        //If audio
        if ($ifaudio == "true") {
            $audiolinkformat = "audio/";
            $filelink = "$youtubeID.$audioformat";
        }
        else {
            $filelink = "video/$youtubeID.$videoformat";
        }

        //Create link
        $filedownload = $startlink.$audiolinkformat.$filelink;

    //Echo stuff
    if (empty($stderr)) {
        echo "
        <div class='dl-content'>
        <h2>$title</h2>
        <a class='dl-link' href='$filedownload' download><span>Download</span>
        <div class='dl-imgcont'><img src='$thumbnail' alt='Thumbnail'></div></a>
        </div>
        ";
    }
    elseif (strpos($stderr,'requested')) {
        exec('youtube-dl "'.$url.'" -o "/var/www/html/ytdl/video/%(id)s.%(ext)s"');
        echo "
        <div class='dl-content'>
        <h2>$title</h2>
        <a class='dl-link' href='$filedownload' download>
        <div class ='hd-error'>No HD avalible! Downloaded best possible quality instead.</div>
        <span>Download</span>
        <div class='dl-imgcont'><img src='$thumbnail' alt='Thumbnail'></div></a>
        </div>
        ";
    }
    else {
        echo "<div class='error-box'><br>$stderr<br></div>";
    }
    //Download the file with headers ((If Ajax != working))    
    //header("Content-Disposition: attachment; filename=\"" . basename($filedownload) . "\"");
    //header("Content-Type: application/force-download");
    //header("Content-Length: " . filesize($filedownload));
    //header("Connection: close");

?>