<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>YTDL</title>
  <link rel="stylesheet" type="text/css" href="res/loading.css">
  <link rel="stylesheet" type="text/css" href="res/animate.css">
  <link rel="stylesheet" type="text/css" href="style.css">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <!--Ajax-->
  <script>
  $(function() {
    $("#dl-form").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function() {
                $("#loader").toggleClass("toggle");
            },
            success: function(data) {
                $("#loader").toggleClass("toggle");
                $("#response").append(data);
                $('html, body').animate({
                    scrollTop: $("#response .dl-content:last").offset().top
                }, 500);
            }
        });
    });
  });
  </script>
</head>
<body>
  <div id="loader">
  <!--<div class="loader-message">This is a message!</div>-->
  <div class='cover'></div>
  <div class='tetrominos'><div class='tetromino box1'></div><div class='tetromino box2'></div><div class='tetromino box3'></div><div class='tetromino box4'></div></div>
  </div>
  <header>
    <a href="/ytdl"><h1><span>YouTube</span>-DL</h1></a>
  </header>

  <nav>
    <ul>
      <li><a href="/ytdl">HOME</a></li>
      <li><a href="mp3">CONVERT MP3</a></li>
      <li><a href="mirror">MIRROR</a></li>
    </ul>
  </nav>

  <main>
    <div class="mainInner">
      <form method="POST" action="download.php" id="dl-form" class="clearfix">
        <!--URL-->
        <input id="url" type="url" placeholder="YouTube URL" name="url" required>
        
        <!--HD-->
        <input type="checkbox" id="hd-check" name="hd" value="true">
        <label for="hd-check" id="hd-lable" title="Download video in full HD">1080p</label>
        
        <!--AUDIO-->
        <input type="checkbox" id="audio-on" name="audio" value="true">
        <label for="audio-on" id="audio-lable" title="Download only audio">Audio Only</label>
        <select id="audioformat" name="audioformat">
          <option value="m4a">Best Quality</option>
          <option value="mp3">mp3</option>          
          <option value="m4a">m4a</option>          
          <option value="wav">wav</option>
          <option value="aac">aac</option>
          <option value="ogg">ogg/vorbis</option>
          <option value="opus">opus</option>
        </select>

        <!--FIXFLOAT /lazy-->
        <div class="fixfloat"></div>
        
        <!--RECODE-->
        <input type="checkbox" id="video-on" name="video" value="true">
        <label for="video-on" id="video-lable" title="Download video in different file types">Recode Video</label>
        <select id="videoformat" name="videoformat">
          <option value="mp4">Best Quality</option>
          <option value="mp4">mp4</option>          
          <option value="flv">flv</option>          
          <option value="ogg">ogg</option>
          <option value="webm">webm</option>
        </select>

        <!--FIXFLOAT /lazy-->        
        <div class="fixfloat"></div>

        <!--SUBMIT-->
        <input id="submit" type="submit" value="Convert">
      </form>
    </div>
  </main>

  <section id="response" class=""></section>

  <section id="about">
    <div class="aboutInner">
      <?php include ("res/about.php"); ?>
      <br><a class="builtBy" href="http://ghostops.nu">Built by Ludvig "GhostOps" Larsendahl</a>
    </div>
  </section>

  <!--Casual JQuery-->
  <script src="res/wow.js"></script>
  <script>
  //Init WOW
  new WOW().init();         

  //Events
  $(document).ready(function(){
      $('#audio-lable').click(function(){
          $(this).toggleClass("toggle");
          $('#audioformat').toggleClass("toggle");
          $("#video-lable").toggleClass("off");
          $("#videoformat").toggleClass("off");
          $("#hd-lable").toggleClass("off");
          $('#hd-check').attr('checked', false);
          $('#videoformat').toggleAttr("disabled", true);
      });
      $('#video-lable').click(function(){
          $(this).toggleClass("toggle");
          $('#videoformat').toggleClass("toggle");
      });
      $('#hd-lable').click(function(){
          $(this).toggleClass("toggle");
          $("#video-lable").toggleClass("off");
          $("#videoformat").toggleClass("off");
          $('#video-on').attr('checked', false);
          $('#videoformat').prop('selectedIndex',0);
      });
  });
  </script>
</body>
</html>