<?php 
    //POST
    $url = $_POST['url'];
    $rand = $_POST['publicID'];

   if (strpos($url,'&')) {
        echo "<div class='error-box'>Sorry, we do not support playlists yet :(<br>If this was not a playlist link, try copy-pasting it again!</div>";
        exit(0);
    }

    //Get Youtube ID from download URL
    $youtubeID = substr($url, strpos($url, "=") + 1); 

    //Define
    $result = array();
    $status;
    //Admin set vars
    if (filter_var($url, FILTER_VALIDATE_URL)) {
        mkdir("random/$rand", 0777);
    }

    //Define url and string to execute
    $string = 'youtube-dl "'.$url.'" -o "/var/www/html/ytdl/mirror/random/'.$rand.'/%(id)s.%(ext)s"';
    $thumbnail = exec("youtube-dl $url --get-thumbnail");

    //Open pipe to recive errors and output
    $descriptorspec = array(
       0 => array("pipe", "r"),  // stdin
       1 => array("pipe", "w"),  // stdout
       2 => array("pipe", "w"),  // stderr
    );
    //Execute
    $process = proc_open($string, $descriptorspec, $pipes);

    //Read from stddout and stderr
    $stdout = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[2]);
    $ret = proc_close($process);

    //Generate JSON Array
    json_encode(array('status' => $ret, 'errors' => $stderr,'url_orginal'=>$url, 'output' => $stdout,'command' => $string));
    $title = exec("youtube-dl $url -e");   

        //Display URL based on filetype 
        $serverURL = $_SERVER['REQUEST_URI']; //Returns the current URL
        $parts = explode('/',$serverURL);
        $dir = $_SERVER['SERVER_NAME'];
        for ($i = 0; $i < count($parts) - 1; $i++) {
            $dir .= $parts[$i] . "/";
        }
        $startlink = "http://$dir";

        $filelink = "random/$rand/$rand-static.html";

        //Create link
        $filedownload = $startlink.$filelink;

    //Echo stuff
    if (empty($stderr)) {
        echo "
            <div class='dl-content'>
            <h2>$title - Mirror</h2>
            <a class='dl-link' href='$filedownload'><span>Link to Mirror</span>
            <div class='dl-imgcont'><img src='$thumbnail' alt='Thumbnail'></div></a>
            $youtubeID
            </div>
        ";
        $htmlcontent = "
            <!DOCTYPE html>
            <html>
            <head>
            <title>$title Mirror@YTDL</title>
                <link rel='stylesheet' type='text/css' href='../../../res/html-static.css'>
            </head>
            <body>
            <a href='/ytdl'><header><h1>YouTube-DL Mirror</h1></header></a>
            <div class='wrapper'>
            <video controls>
            <source src='$youtubeID.mp4' type='video/mp4'>
            <a href='$youtubeID.mp4'>Download</a>
            </video>
            </div>

            </body>
            </html>
        ";
        exec("cat > random/$rand/$rand-static.html");
        exec("echo '".$htmlcontent."' > random/$rand/$rand-static.html");
    }
    else {
        echo "<div class='error-box'><br>$stderr<br></div>";
    }
?>