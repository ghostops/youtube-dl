<?php
  //Generate publicID
  $publicID = substr(md5(rand()), 0, 7);

  //Get server URL
  $serverURL = $_SERVER['REQUEST_URI']; //Returns the current URL
  $parts = explode('/',$serverURL);
  $dir = $_SERVER['SERVER_NAME'];
  for ($i = 0; $i < count($parts) - 1; $i++) {
      $dir .= $parts[$i] . "/";
  }
  $startlink = "http://$dir";

  $folderURL = $startlink.'random/'.$publicID;

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>YTDL</title>
  <link rel="stylesheet" type="text/css" href="../res/loading.css">
  <link rel="stylesheet" type="text/css" href="../res/animate.css">
  <link rel="stylesheet" type="text/css" href="style.css">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <!--Ajax-->
  <script>
  $(function() {
    $("#dl-form").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function() {
                $("#loader").toggleClass("toggle");
            },
            success: function(data) {
                $("#loader").toggleClass("toggle");
                $("#response").append(data);
                $('html, body').animate({
                    scrollTop: $("#response .dl-content:last").offset().top
                }, 500);
            }
        });
    });
  });
  </script>
</head>
<body>
  <div id="loader">
  <!--<div class="loader-message">This is a message!</div>-->
  <div class='cover'></div>
  <div class='tetrominos'><div class='tetromino box1'></div><div class='tetromino box2'></div><div class='tetromino box3'></div><div class='tetromino box4'></div></div>
  </div>
  <header>
    <a href="/ytdl"><h1><span>YouTube</span> - <strong>MIRROR</strong></h1></a>
  </header>

  <nav>
    <ul>
      <li><a href="/ytdl">HOME</a></li>
      <li><a href="../mp3">CONVERT MP3</a></li>
      <li><a href="#">MIRROR</a></li>
    </ul>
  </nav>

  <main>
    <div class="mainInner">
    <p>A mirror will last for up to 12 weeks.</p>
      <form method="POST" action="mirror.php" id="dl-form" class="clearfix">
        <!--URL-->
        <input id="url" type="url" placeholder="YouTube URL to mirror" name="url">

        <!--ID-->
        <input class="hidden-forms" type="text" name="publicID" value="<?php echo $publicID ?>"><!--You will only fuck your shit up if you change this...-->

        <!--SUBMIT-->
        <input id="submit" type="submit" value="Convert">
      </form>
    </div>
  </main>

  <section id="response" class=""></section>

  <section id="about">
    <div class="aboutInner">
      <?php include ("../res/about.php"); ?>
      <br><a class="builtBy" href="http://ghostops.nu">Built by Ludvig "GhostOps" Larsendahl</a>
    </div>
  </section>

  <!--Casual JQuery-->
  <script src="../res/wow.js"></script>
  <script>
  //Init WOW
  new WOW().init();         

  //Events
  $(document).ready(function(){
      $('#audio-lable').click(function(){
          $(this).toggleClass("toggle");
          $('#audioformat').toggleClass("toggle");
          $("#video-lable").toggleClass("off");
          $("#videoformat").toggleClass("off");
          $("#hd-lable").toggleClass("off");
          $('#hd-check').attr('checked', false);
          $('#videoformat').toggleAttr("disabled", true);
      });
      $('#video-lable').click(function(){
          $(this).toggleClass("toggle");
          $('#videoformat').toggleClass("toggle");
      });
      $('#hd-lable').click(function(){
          $(this).toggleClass("toggle");
          $("#video-lable").toggleClass("off");
          $("#videoformat").toggleClass("off");
          $('#video-on').attr('checked', false);
          $('#videoformat').prop('selectedIndex',0);
      });
  });
  </script>
</body>
</html>